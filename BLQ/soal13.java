package jawaban;

import java.util.Scanner;

public class soal13 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		double jam = 5, menit = 30;

		if (jam < 0 || menit < 0 || jam > 12 || menit > 60)
			System.out.println("Wrong input");

		if (jam == 12)
			jam = 0;
		if (menit == 60) {
			menit = 0;
			jam += 1;
			if (jam > 12)
				jam = jam - 12;
		}

		int derajatMenit = (int) (0.5 * (jam * 60 + menit));
		int derajatJam = (int) (6 * menit);
		int derajatWaktu = Math.abs(derajatJam - derajatMenit);
		derajatWaktu = Math.min(360 - derajatWaktu, derajatWaktu);
		
		System.out.println(derajatWaktu);

	}

}
