package jawaban;

import java.util.Scanner;

public class soal14 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner inputSoal = new Scanner(System.in);
		System.out.println("Deret: 3 9 0 7 1 2 4");
		int[] deret = { 3, 9, 0, 7, 1, 2, 4 };
		System.out.print("Input N: ");
		int inputN = inputSoal.nextInt();
		int[] dummyArray = new int[inputN];
		System.out.print("Hasil:");
		int temporary, counter = 0;

		if (inputN % deret.length == 0) {
			for (int i = 0; i < deret.length; i++) {
				System.out.print(" " + deret[i]);
			}
		} else {
			if (inputN > deret.length)
				inputN = inputN % deret.length;
			for (int i = 0; i < inputN; i++) {
				dummyArray[i] = deret[i];
			}
			for (int i = 0; i < deret.length; i++) {
				if (deret.length - i > inputN) {
					temporary = deret[i];
					deret[i] = deret[i + inputN];
					deret[i + inputN] = temporary;
				} else {
					deret[i] = dummyArray[counter];
					counter++;
				}

				System.out.print(" " + deret[i]);

			}
		}

	}

}
