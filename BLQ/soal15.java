package jawaban;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Scanner;

public class soal15 {

	public static void main(String[] args) throws ParseException{
		// TODO Auto-generated method stub
		Scanner inputSoal = new Scanner(System.in);
		System.out.print("Input jam (Ex 03:40:44 AM/PM): ");
		String waktu = inputSoal.nextLine();
		SimpleDateFormat displayFormat = new SimpleDateFormat("HH:mm:ss");
		SimpleDateFormat parseFormat = new SimpleDateFormat("hh:mm:ss a");
		Date date = parseFormat.parse(waktu);
		System.out.println(displayFormat.format(date));

	}

}
