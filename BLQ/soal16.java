package jawaban;

import java.util.Scanner;

public class soal16 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner inputSoal = new Scanner (System.in);
		System.out.print("Tuna Sandwich: ");
		int tunaSandwich = inputSoal.nextInt();
		double totalTuna = ((tunaSandwich * 0.15) + tunaSandwich) / 3;
		System.out.print("Spaghetti Carbonara: ");
		int spaghettiCarbonara = inputSoal.nextInt();
		double totalSpaghetti = ((spaghettiCarbonara * 0.15) + spaghettiCarbonara) / 4;
		System.out.print("Tea Pitcher: ");
		int teaPitcher = inputSoal.nextInt();
		double totalTea = ((teaPitcher * 0.15) + teaPitcher) / 4;
		System.out.print("Pizza: ");
		int pizzaMenu = inputSoal.nextInt();
		double totalPizza = ((pizzaMenu * 0.15) + pizzaMenu) / 4;
		System.out.print("Salad: ");
		int saladMenu = inputSoal.nextInt();
		double totalSalad = ((saladMenu * 0.15) + saladMenu) / 4;

		System.out.println(
				"Total harga untuk yang alergi ikan: " + (int) (totalSpaghetti + totalTea + totalPizza + totalSalad));
		System.out.println("Total harga untuk yang lainnya: "
				+ (int) (totalTuna + totalSpaghetti + totalTea + totalPizza + totalSalad));

	}

}
