package jawaban;

import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.lang.*;
import java.time.*;
import java.util.*;
import java.text.*;
import java.time.*;
import java.time.format.DateTimeParseException;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;


public class soal2 {

	public static void main(String[] args) throws ParseException {
		
		Scanner Jawaban = new Scanner(System.in);
		String Date = "dd MMMM yyyy";
		SimpleDateFormat dateFormat = new SimpleDateFormat(Date, new Locale("id", "ID"));
		System.out.print("Tanggal pinjam (Ex:17 Desember 2023): ");
		String inputDatePinjam = Jawaban.nextLine();
		System.out.print("Tanggal pengembalian (Ex:23 Desember 2023): ");
		String inputDatePengembalian = Jawaban.nextLine();
		System.out.print("Nama Buku: ");
		String inputBuku = Jawaban.nextLine();
		long tanggalDenda, totalDenda;

		Date datepinjam = dateFormat.parse(inputDatePinjam);
		Date datekembali = dateFormat.parse(inputDatePengembalian);
		LocalDate date = datepinjam.toInstant().atZone(java.time.ZoneId.systemDefault()).toLocalDate();
		LocalDate date2 = datekembali.toInstant().atZone(java.time.ZoneId.systemDefault()).toLocalDate();
		int selisihConverter = (int) ChronoUnit.DAYS.between(date, date2);
		switch (inputBuku) {
			case "A": {
				tanggalDenda = selisihConverter - 14;
				totalDenda = tanggalDenda * 100;
				if (totalDenda >= 0) {
					System.out.println("Total denda: " + totalDenda);
				} else {
					System.out.print("Total denda: 0");
				}
			break;
			}
			case "B": {
				tanggalDenda = selisihConverter - 3;
				totalDenda = tanggalDenda * 100;
				if (totalDenda >= 0) {
					System.out.println("Total denda: " + totalDenda);
				} else {
					System.out.print("Total denda: 0");
			}
			break;
			}
			case "C": {
				tanggalDenda = selisihConverter - 7;
				totalDenda = tanggalDenda * 100;
				if (totalDenda >= 0) {
					System.out.println("Total denda: " + totalDenda);
				} else {
					System.out.print("Total denda: 0");
				}
			break;
			}
			case "D": {
				tanggalDenda = selisihConverter - 7;
				totalDenda = tanggalDenda * 100;
				if (totalDenda >= 0) {
					System.out.println("Total denda: " + totalDenda);
				} else {
					System.out.print("Total denda: 0");
			}
			break;
			}
			default:
				throw new IllegalArgumentException("Unexpected value: " + inputBuku);
		}

  }
    
}

