package jawaban;

import java.util.Scanner;

public class soal5 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner fibonaci = new Scanner(System.in);
	    System.out.print("Masukan N : ");
	    int inputan = fibonaci.nextInt();
	    int[] a = new int[inputan];

	    int kondisi1 = 0, kondisi2 = 1;
	    System.out.println("Menampilkan deret fibonaci " + inputan + " suku:");

	    for (int i = 1; i <= inputan; ++i) {
	      a[i-1] = kondisi1;

	      if (i == inputan){
	        System.out.println(kondisi1);
	      }else
	      {
	        System.out.print(kondisi1 + ", ");
	      }
	      

	      // compute the next term
	      int kondisi3 = kondisi1 + kondisi2;
	      kondisi1 = kondisi2;
	      kondisi2 = kondisi3;
	    }
	    System.out.println("Bilangan Fibonaci Pertama:"+a[0]);

	}

}
