package jawaban;

import java.util.Arrays;
import java.util.Scanner;

public class soal8 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner inputSoal = new Scanner(System.in);
		System.out.print("Input deret angka: ");
		String deretAngka1 = inputSoal.nextLine();
		String[] deretArray = deretAngka1.split(" ");
		int[] deretAngka = new int[deretArray.length];

		for (int i = 0; i < deretArray.length; i++) {
			deretAngka[i] = Integer.parseInt(deretArray[i]);
		}

		int maxNilai = 0, minNilai = 0;
		Arrays.sort(deretAngka);
		int n = 0;
		for (int i = deretAngka.length - 1; i >= 0; i--) {
			if (n < 4) {
				maxNilai += deretAngka[i];
				n++;
			}
			if (i < 4) {
				minNilai += deretAngka[i];
			}
		}

		System.out.println("Nilai min: " + minNilai);
		System.out.println("Nilai max: " + maxNilai);

	}

}
